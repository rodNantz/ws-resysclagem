package resysclagem.ws.testcases;

import static org.junit.Assert.*;

import java.awt.Container;

import javax.ws.rs.ProcessingException;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.junit.Test;

import junit.framework.AssertionFailedError;
import resysclagem.ws.classes.Administrador;
import resysclagem.ws.services.*;
import resysclagem.ws.services.AdminWS.ContaAdmin;

public class AdminTest {

	@Test
	public void testLoginValido() {
    	try {
	    	WebTarget webTarget = AppTest.client.target(AppTest.endpoint + "/admin/session");
	    	Response response = webTarget
	    							.request(MediaType.APPLICATION_JSON)
	    							.post(
	    								Entity.entity(new Administrador("resysadmin@gmail.com", "resysadmin*$"), MediaType.APPLICATION_JSON)
	    								);
	    	if (String.valueOf(response.getStatus()).startsWith("2")) {
	    		ContaAdmin cadmin = AppTest.gson.fromJson( response.readEntity(String.class),
	    												   ContaAdmin.class);
	    		boolean valido = (cadmin.getLoginExists() && cadmin.getPasswordConfirmed());
	    		assertTrue(valido);
	    	} else {
	    		fail("HTTP " + String.valueOf(response.getStatus()));
	    	}
    	} catch (Exception e){
    		fail(e.getMessage());
    	}	
	}

}

package resysclagem.ws.testcases;

import static org.junit.Assert.*;

import java.awt.Container;
import java.util.Calendar;

import javax.ws.rs.ProcessingException;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.junit.Test;

import com.google.gson.JsonObject;

import junit.framework.AssertionFailedError;
import resysclagem.ws.classes.Administrador;
import resysclagem.ws.classes.MsgResponse;
import resysclagem.ws.classes.Request;
import resysclagem.ws.services.*;
import resysclagem.ws.services.AdminWS.ContaAdmin;

public class RelatorioTest {
	
	/**
	 * nesse teste, sem insert em banco
	 */
	@Test
	public void testGerarRelatorio() {
		
    	try {
    		JsonObject jsonRel = new JsonObject();
    		jsonRel.addProperty("codRel", 0);
    		Calendar cal = Calendar.getInstance();
    		cal.add(Calendar.DATE, 1);
    		jsonRel.addProperty("dataE", cal.getTime().toString());
    		jsonRel.addProperty("nome", "Relatório gerado do UT");
    		
	    	WebTarget webTarget = AppTest.client.target(AppTest.endpoint + "/relatorio");
	    	Response response = webTarget
	    							.request(MediaType.APPLICATION_JSON)
	    							.post(
	    								Entity.entity(new Request(AppTest.gson.toJson(jsonRel), "aidi", "toqui"), MediaType.APPLICATION_JSON)
	    								);
	    	if (String.valueOf(response.getStatus()).startsWith("2")) {
	    		MsgResponse msg = AppTest.gson.fromJson( response.readEntity(String.class),
	    												 MsgResponse.class);
	    		if(!msg.isStatus()){
	    			System.err.println(msg.getMessage());
	    		}
	    		assertTrue(msg.isStatus());
	    	} else {
	    		fail("HTTP " + String.valueOf(response.getStatus()));
	    	}
    	} catch (Exception e){
    		e.printStackTrace();
    		fail(e.getMessage());
    	} finally {
    		
    	}
    	
	}

}

package resysclagem.ws;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import resysclagem.ws.testcases.AdminTest;
import resysclagem.ws.testcases.AppTest;
import resysclagem.ws.testcases.PontoColetaTest;
import resysclagem.ws.testcases.RelatorioTest;
import resysclagem.ws.testcases.TokenTest;

@RunWith(Suite.class)
@SuiteClasses({ AppTest.class, AdminTest.class, PontoColetaTest.class, TokenTest.class, RelatorioTest.class })
public class AllTests {

}

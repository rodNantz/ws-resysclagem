package resysclagem.ws.standalonetests;

import java.io.IOException;

import org.junit.Test;

import junit.framework.TestCase;
import resysclagem.ws.PropertySource;
import resysclagem.ws.config.jobs.DatabaseBackup;

public class DatabaseBackupRun extends TestCase {

	@Test
	public static void main(String[] args) throws IOException {
		Boolean prod = Boolean.valueOf(PropertySource.props.getProperty("resysclagem.launch.prod"));
		String sufixoProd = "";
		
		if(prod){
			System.out.println("* Production mode ON (Prod mode)");
			sufixoProd = ".prod";
		} else {
			System.out.println("* Not prod application (Dev mode)");
		}
		
		String dbUsuario = PropertySource.props.getProperty("resysclagem.bd"+ sufixoProd +".usuario");
        String dbSenha = PropertySource.props.getProperty("resysclagem.bd"+ sufixoProd +".senha"); // pensar em em maneira de encriptar
        String dbFullUrl = PropertySource.props.getProperty("resysclagem.bd"+ sufixoProd +".url");
        
        try {
        	boolean done = DatabaseBackup.doBackup(dbUsuario, dbSenha, dbFullUrl, "resysclagem");
        	assertTrue(done);
        } catch (Exception e){
        	e.printStackTrace();
        }
	}
	

}

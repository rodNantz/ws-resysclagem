package resysclagem.ws.dao;

import java.lang.reflect.Type;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import resysclagem.ws.classes.KeyValue;
import resysclagem.ws.classes.Relatorio;
import resysclagem.ws.classes.Residuo;

public class RelatorioDAO {
	
	Gson gson = new Gson();
	Type listGenType = new TypeToken<List<?>>(){}.getType();
	
	private final String stmtListarTR = "SELECT * FROM tipo_relatorio";
	
	/*
	 * RelatorioStmt:
	 * - $AscOrDesc : replace por ASC ou DESC (chacar se um dos dois pra evitar injection)
	 * 1: dataInicial
	 * 2: dataFInal
	 * 
	 * 
	 */
	
    public List<KeyValue> listarTiposRelatorio() throws SQLException, Exception {
        Connection con = null;
        PreparedStatement stmt = null;
        List<KeyValue> tipos = new ArrayList<>();
        try{
            con = ConnectionFactory.getConnection();
            stmt = con.prepareStatement(stmtListarTR);
            stmt.executeQuery();
            ResultSet rs = stmt.getResultSet();
            while (rs.next()){
            	KeyValue kv = new KeyValue(rs.getInt("cod_rel"), rs.getString("nome"), 
            							   rs.getString("stmt_relatorio"), rs.getBoolean("ignora_datas"));
            	tipos.add(kv);
            }
            
            return tipos;
        } catch (SQLException ex) {
        	ex.printStackTrace();
            throw new SQLException("Erro ao buscar tipos de relatório. Origem: "+ex.getMessage());
        } finally{
            try{stmt.close();}catch(Exception ex){System.out.println("Erro ao fechar stmt. Ex="+ex.getMessage());};
            try{con.close();;}catch(Exception ex){System.out.println("Erro ao fechar conexão. Ex="+ex.getMessage());};
        }
    }
    
    public Relatorio preGerarRelatorio(Relatorio relatorio) throws SQLException, Exception {
    	String replacement = "";
        if(relatorio.getOrdem().trim().equalsIgnoreCase("ASC") || relatorio.getOrdem().trim().equalsIgnoreCase("DESC"))
        	replacement = relatorio.getOrdem();
        String query = relatorio.getQuery().replace("$AscOrDesc", replacement);
        relatorio.setQuery(query);
        try{
            if(! relatorio.isIgnoraDatas()) {
            	return gerarRelatorioDatas(relatorio);
            } else {
            	return gerarRelatorioSemDatas(relatorio);
            }
        } catch (Exception e){
        	throw e;
        }
    }
    
    public Relatorio gerarRelatorioDatas(Relatorio relatorio) throws SQLException, Exception {
        Connection con = null;
        PreparedStatement stmt = null;
        try{
            con = ConnectionFactory.getConnection();
            stmt = con.prepareStatement(relatorio.getQuery());
            
            stmt.setString(1, relatorio.getDataInicial());
            stmt.setString(2, relatorio.getDataFinal());
            // System.err.println(stmt.toString());
            
            stmt.executeQuery();
            
            List<String> v = new ArrayList<>();
            StringBuilder h = new StringBuilder();
            h.append("[{\"data\": [");
            ResultSet rs = stmt.getResultSet();
            while (rs.next()){
            	// retorna: vertical (cod), nome, horizontal (valor count)
            	v.add(rs.getString("nome"));
            	h.append(rs.getInt("horizontal"));
            	if(!rs.isLast()) h.append(",");
            }
            h.append("], \"label\": \"Dados\"}]");
            
            
            relatorio.setDadoVertical(v);
            relatorio.setDadoHorizontal( gson.fromJson(h.toString(), listGenType) );
            
            return relatorio;
        } catch (SQLException ex) {
        	System.err.println(stmt);
        	ex.printStackTrace();
            throw new SQLException("Erro ae buscar relatório. Origem: "+ex.getMessage());
        } finally{
            try{stmt.close();}catch(Exception ex){System.out.println("Erro ao fechar stmt. Ex="+ex.getMessage());};
            try{con.close();;}catch(Exception ex){System.out.println("Erro ao fechar conexão. Ex="+ex.getMessage());};
        }
    }    
    
    
    public Relatorio gerarRelatorioSemDatas(Relatorio relatorio) throws SQLException, Exception {
        Connection con = null;
        PreparedStatement stmt = null;
        try{
            con = ConnectionFactory.getConnection();
            stmt = con.prepareStatement(relatorio.getQuery());
            System.err.println(stmt.toString());
            stmt.executeQuery();
            
            List<String> v = new ArrayList<>();
            StringBuilder h = new StringBuilder();
            h.append("[{\"data\": [");
            ResultSet rs = stmt.getResultSet();
            while (rs.next()){
            	// retorna: vertical (cod), nome, horizontal (valor count)
            	v.add(rs.getString("nome"));
            	h.append(rs.getInt("horizontal"));
            	if(!rs.isLast()) h.append(",");
            }
            h.append("], \"label\": \"Dados\"}]");
            
            
            relatorio.setDadoVertical(v);
            relatorio.setDadoHorizontal( gson.fromJson(h.toString(), listGenType) );
            
            return relatorio;
        } catch (SQLException ex) {
        	System.err.println(stmt);
        	ex.printStackTrace();
            throw new SQLException("Erro ae buscar relatório. Origem: "+ex.getMessage());
        } finally{
            try{stmt.close();}catch(Exception ex){System.out.println("Erro ao fechar stmt. Ex="+ex.getMessage());};
            try{con.close();;}catch(Exception ex){System.out.println("Erro ao fechar conexão. Ex="+ex.getMessage());};
        }
    }
    
}

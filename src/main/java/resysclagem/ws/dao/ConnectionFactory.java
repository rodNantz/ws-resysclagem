package resysclagem.ws.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public final class ConnectionFactory {
	
	private static String url 		= null;
    private static String usuario 	= null; 	
    private static String senha 	= null; 	
	
    public static void setParameters(String url, String usuario, String senha) throws SQLException, ClassNotFoundException{
        ConnectionFactory.url 		= 	url;
        ConnectionFactory.usuario 	= 	usuario;
        ConnectionFactory.senha 	= 	senha;  
    }
    
    public static Connection getConnection(String url, String usuario, String senha) throws SQLException, ClassNotFoundException{
        //Class.forName("com.mysql.jdbc.Driver");
    	Class.forName("com.mysql.cj.jdbc.Driver");
        Connection con;  
        con = DriverManager.getConnection(url, usuario, senha);  
        return con;
    }
    
	public static Connection getConnection() throws SQLException, ClassNotFoundException{
        if (url == null)
        	throw new NullPointerException("Parameters were not set! Use ConnectionFactory.setParameters()");       
        return getConnection(url, usuario, senha);
    }
}

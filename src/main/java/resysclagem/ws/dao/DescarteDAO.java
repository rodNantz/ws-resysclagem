package resysclagem.ws.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.text.AttributeSet.CharacterAttribute;

import com.google.gson.Gson;

import resysclagem.ws.classes.Categoria;
import resysclagem.ws.classes.Descarte;
import resysclagem.ws.classes.PontoColeta;
import resysclagem.ws.classes.Residuo;
import resysclagem.ws.classes.Usuario;

public class DescarteDAO {
	//descarte: codd, datad, usuario_codu1, pontocoleta_codpc1, statusd
	
	private final String stmtInserirD = "INSERT INTO descarte (datad, usuario_codu1, pontocoleta_codpc1, statusd) "
			+ "VALUES (?, ?, ?, ?)";
    
	private final String stmtInserirDesHasRes = "INSERT INTO descarte_has_residuo (descarte_codd, residuo_codr) "
			+ "VALUES (?, ?)";
	
	private final String stmtAlterarD = "UPDATE descarte SET datad=?, usuario_codu1=?, pontocoleta_codpc1=?, statusd=? "
			+ "WHERE codd=?";
    
    private final String stmtRemoverD = "DELETE FROM descarte WHERE codd=?";
    
    private final String stmtListarD = "SELECT * FROM descarte WHERE usuario_codu1=?";
    
    private final String stmtBuscaCodD = "SELECT * FROM descarte WHERE codd=?";
    
    private final String stmtBuscaListResComDescarte = "SELECT * FROM descarte_has_residuo WHERE descarte_codd=?";
    
    private final String stmtDescartesDoUsu = "SELECT * FROM descarte WHERE usuario_codu1=?";
    
    public boolean inserirDescarte(Descarte des) throws SQLException, Exception {
        Connection con = null;
        PreparedStatement stmt = null;
        try {
            con = ConnectionFactory.getConnection();
            con.setAutoCommit(false);
            stmt = con.prepareStatement(stmtInserirD, PreparedStatement.RETURN_GENERATED_KEYS);
            stmt.setString(1, des.getDataDB());
            stmt.setInt(2, des.getUsuario().getCodU());
            stmt.setInt(3, des.getPontoColeta().getCodPC());
            stmt.setBoolean(4, des.isStatusD());
            stmt.executeUpdate();
            con.commit();
            
            ResultSet rs = stmt.getGeneratedKeys();  
            if(rs.next())
            	des.setCodD(rs.getInt(1));
            rs.close();
            
            inserirDescarteHasResiduo(des);
            return true;
        } catch (SQLException ex) {
            try{con.rollback();}
            catch(SQLException ex1) {
            	throw new SQLException("Erro ao tentar rollback. Ex="+ex1.getMessage());
            	};
            System.err.println(new Gson().toJson(des));
            System.err.println(stmt.toString());
            throw new SQLException("Erro ao inserir um descarte. Origem: " + ex.getMessage());
        } finally{
            try{stmt.close();}catch(Exception ex){
            	throw new Exception("Erro ao fechar stmt. Ex="+ex.getMessage());
            	};
            try{con.close();;}catch(Exception ex){
            	throw new Exception("Erro ao fechar conexão. Ex="+ex.getMessage());
            	};
        }
    }
    
    public boolean inserirDescarteHasResiduo(Descarte des) throws SQLException, ClassNotFoundException, Exception {
    	Connection con = null;
        PreparedStatement stmt = null;
        try {
            con = ConnectionFactory.getConnection();
            con.setAutoCommit(false);
            stmt = con.prepareStatement(stmtInserirDesHasRes);
        	int i = 0;
        	if(des.getResiduos() != null){
	        	for(Residuo res : des.getResiduos()){
		            stmt.setInt(1, des.getCodD());
		            stmt.setInt(2, res.getCodR());
		            stmt.addBatch();
	                if (i % 100 == 0 || i == des.getResiduos().size()) {
	                    stmt.executeBatch(); // Execute every 100 items.
	                }
	        	}
	        	con.commit();
        	} else {
        		System.out.println("WARN: descarte "+ des.getCodD() +" não tem resíduos");
        	}
            return true;
        } catch (SQLException ex) {
            try{con.rollback();}
            catch(SQLException ex1) {
            	ex1.printStackTrace();
            	throw new SQLException("Erro ao tentar rollback. Ex="+ex1.getMessage());
            	};
            
            throw new SQLException("Erro ao inserir em descarte_has_residuo. Origem: " + ex.getMessage());
        } finally{
            try{stmt.close();}catch(Exception ex){
            	ex.printStackTrace();
            	throw new Exception("Erro ao fechar stmt. Ex="+ex.getMessage());
            	};
            try{con.close();;}catch(Exception ex){
            	ex.printStackTrace();
            	throw new Exception("Erro ao fechar conexão. Ex="+ex.getMessage());
            	};
        }
    }
    
    public boolean alterarDescarte(Descarte des) throws SQLException, ClassNotFoundException, Exception {
        Connection con = null;
        PreparedStatement stmt = null;
        try{
        	con = ConnectionFactory.getConnection();
            //con.setAutoCommit(false);
            stmt = con.prepareStatement(stmtAlterarD, PreparedStatement.RETURN_GENERATED_KEYS);
            stmt.setString(1, des.getDataDB());
            stmt.setInt(2, des.getUsuario().getCodU());
            stmt.setInt(3, des.getPontoColeta().getCodPC());
            stmt.setBoolean(4, des.isStatusD());
            stmt.setInt(5, des.getCodD());
            stmt.executeUpdate();
            
            return true;
        }catch(SQLException ex){
            throw new SQLException("Erro ao alterar o Descarte. Origem: " + ex.getMessage());
        }finally{
            try{stmt.close();}catch(Exception ex){
            	System.out.println("Erro ao fechar stmt. Ex = " +ex.getMessage()); 
            	throw ex;
            	};
            try{con.close();;}catch(Exception ex){
            	System.out.println("Erro ao fechar conexão. Ex= "+ex.getMessage()); 
            	throw ex;
            	};
        }
    }
    
    public boolean removerDescarte(Descarte des) throws SQLException, ClassNotFoundException, Exception {
        Connection con = null;
        PreparedStatement stmt = null;
        try{
            con = ConnectionFactory.getConnection();
            stmt = con.prepareStatement(stmtBuscaListResComDescarte);
            stmt.setInt(1, des.getCodD());
            stmt.executeUpdate();
            return true;
        }catch(SQLException ex){
            throw new SQLException("Erro ao deletar esse descarte. Origem: " +ex.getMessage());
        }finally{
            try{stmt.close();}catch(Exception ex){
            	System.out.println("Erro ao fechar stmt. Ex="+ex.getMessage()); 
            	throw ex;
            	};
            try{con.close();;}catch(Exception ex){
            	System.out.println("Erro ao fechar conexão. Ex="+ex.getMessage()); 
            	throw ex;
            	};                
        }
    }
    
    //read
    public List<Descarte> listarDescartes(Usuario usuario) throws SQLException, ClassNotFoundException, Exception {
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        List<Descarte> lista = new ArrayList<Descarte>();
        UsuarioDAO usuDAO = new UsuarioDAO();
        PontoColetaDAO pcoDAO = new PontoColetaDAO();
        try {
            con = ConnectionFactory.getConnection();
	        stmt = con.prepareStatement(stmtListarD);
	        stmt.setInt(1, usuario.getCodU());
	        rs = stmt.executeQuery();
	        while(rs.next()){
	        	Usuario u = usuDAO.consultarUsuarioPorCod( rs.getInt("usuario_codu1") );
	        	PontoColeta pc = pcoDAO.pesquisarPCO( rs.getInt("pontocoleta_codpc1") );
	    		Descarte des = new Descarte(
	    	            rs.getInt("codd"),
	    	            u,
	    	            pc,
	    	            rs.getString("datad"),
	    	            rs.getBoolean("statusd"),
	    	            this.listarResiduosDeDescarte( rs.getInt("codd") )
	    				);
	            lista.add(des);
	        }
	        return lista;
        } catch (SQLException e){
        	throw new SQLException(e.getMessage());
        } catch (ClassNotFoundException ex) {
            throw new Exception("Erro ao listar os pontos: " + ex.getMessage());
	    } catch (Exception e) {
	    	e.printStackTrace();
	        throw new Exception(e.getMessage());
	    }
    }
    
    public List<Residuo> listarResiduosDeDescarte(int codD) throws SQLException, ClassNotFoundException, Exception {
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        List<Residuo> lista = new ArrayList<Residuo>();
        ResiduoDAO resDAO = new ResiduoDAO();
        try {
            con = ConnectionFactory.getConnection();
	        stmt = con.prepareStatement(stmtBuscaListResComDescarte);
	        stmt.setInt(1, codD);
	        rs = stmt.executeQuery();
	        while(rs.next()){
	        	Residuo r = resDAO.pesquisarResiduo( rs.getInt("residuo_codr") ); 
	            lista.add(r);
	        }
	        return lista;
        } catch (SQLException e){
        	throw new SQLException(e.getMessage());
        } catch (ClassNotFoundException ex) {
            throw new Exception("Erro ao listar os pontos: " + ex.getMessage());
	    } catch (Exception e) {
	    	e.printStackTrace();
	        throw new Exception(e.getMessage());
	    }
    }
    
    public Descarte pesquisarDescarte(int codPC) throws SQLException, Exception { 
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Descarte des = null;
        UsuarioDAO usuDAO = new UsuarioDAO();
        PontoColetaDAO pcoDAO = new PontoColetaDAO();
        try{
        	con = ConnectionFactory.getConnection();
            stmt = con.prepareStatement(stmtBuscaCodD);
            stmt.setInt(1, codPC);
            rs = stmt.executeQuery();
            if(rs.next()){
	        	Usuario u = usuDAO.consultarUsuarioPorCod( rs.getInt("usuario_codu1") );
	        	PontoColeta pc = pcoDAO.pesquisarPCO( rs.getInt("pontocoleta_codpc1") );
	    		des = new Descarte(
	    	            rs.getInt("codd"),
	    	            u,
	    	            pc,
	    	            rs.getString("datad"),
	    	            rs.getBoolean("statusd"),
	    	            this.listarResiduosDeDescarte( rs.getInt("codd") )
	    	            );
            }
            return des;
        }catch(SQLException ex){
            throw new SQLException("Erro ao pesquisar o descarte. Origem: " +ex.getMessage());
        } catch (Exception e){
        	throw new Exception(e.getMessage());
        }finally{
            try{rs.close();}catch(Exception ex){
            	System.out.println("Erro ao fechar result set. Ex="+ex.getMessage());
            	throw ex;
            	};
            try{stmt.close();}catch(Exception ex){
            	System.out.println("Erro ao fechar stmt. Ex="+ex.getMessage());
            	throw ex;
            	};
            try{con.close();;}catch(Exception ex){
            	System.out.println("Erro ao fechar conexão. Ex="+ex.getMessage());
            	throw ex;
            	};                
        }
    }
    
    public List<Descarte> buscarDescartesDoUsuario(Usuario usuario) throws Exception {
        List<Descarte> lista = new ArrayList<Descarte>();  
		Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        PontoColetaDAO pcoDAO = new PontoColetaDAO();
        UsuarioDAO usuDAO = new UsuarioDAO();
        try {
        	Usuario u = usuDAO.consultarUsuarioPorCod( usuario.getCodU() );
            con = ConnectionFactory.getConnection();
            stmt = con.prepareStatement(stmtDescartesDoUsu);
            stmt.setInt(1, usuario.getCodU());
            rs = stmt.executeQuery();
            while(rs.next()){
            	PontoColeta pc = pcoDAO.pesquisarPCO( rs.getInt("pontocoleta_codpc1") );
	    		Descarte des = new Descarte(
	    	            rs.getInt("codd"),
	    	            u,
	    	            pc,
	    	            rs.getString("datad"),
	    	            rs.getBoolean("statusd"),
	    	            this.listarResiduosDeDescarte( rs.getInt("codd") )
	    	            );
	    		lista.add(des);
            }
            return lista;
        }catch(SQLException ex){
        	ex.printStackTrace();
            throw new SQLException("Erro ao buscar ponto. Origem: " + ex.getMessage());
        } catch (Exception e) {
        	e.printStackTrace();
        	throw new Exception("Erro ao buscar ponto. Origem: " + e.getMessage());
		}finally{
            try {
            	if (rs != null) rs.close();
            } catch(Exception ex) {
            	System.out.println("Erro ao fechar result set. Ex="+ex.getMessage());
            	ex.printStackTrace();
            };
            try {
            	if (stmt != null) stmt.close();
            } catch(Exception ex) {
            	System.out.println("Erro ao fechar stmt. Ex="+ex.getMessage());
            	ex.printStackTrace();
            };
            try{ 
            	if (con != null) con.close();
            } catch(Exception ex) {
            	System.out.println("Erro ao fechar conexão. Ex="+ex.getMessage());
            	ex.printStackTrace();
            };
        }
    }
    
    
}

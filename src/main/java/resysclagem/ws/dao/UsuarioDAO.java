package resysclagem.ws.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import resysclagem.ws.classes.PontoColeta;
import resysclagem.ws.classes.Residuo;
import resysclagem.ws.classes.Usuario;

public class UsuarioDAO {
	private final String stmtConsultar = "SELECT * FROM usuario WHERE email=?";
	private final String stmtLogin = "SELECT * FROM usuario WHERE email=? AND senha=?";
	private final String stmtInserirU = "INSERT INTO usuario (nome, datanasc, email, senha) VALUES (?,?,?,?)";
    private final String stmtAlterarU = "UPDATE usuario SET nome=?, datanasc=?, email=?, senha=? WHERE codu=?";
    private final String stmtRemoverU = "DELETE FROM usuario WHERE codu=?";
    private final String stmtConsultarID = "SELECT * FROM usuario WHERE codu=?";
    private final String stmtInserirFavorito = "INSERT INTO usuario_has_residuo (usuario_codu, residuo_codr) "
												+ "VALUES (?, ?)";
    private final String stmtRemoverFavorito = "DELETE FROM usuario_has_residuo WHERE usuario_codu=? AND residuo_codr=? ";
    private final String stmtListarFavoritos = "SELECT * FROM usuario_has_residuo WHERE usuario_codu=?";
    
    public final String msgResiduoJaFavoritado = "Esse resíduo já foi favoritado!"; 
	        
    public Usuario inserirUsuario(Usuario usuario, boolean returnPassword) throws SQLException, Exception {
        Connection con = null;
        PreparedStatement stmt = null;
        try{
            con = ConnectionFactory.getConnection();
            con.setAutoCommit(false);
            stmt = con.prepareStatement(stmtInserirU,PreparedStatement.RETURN_GENERATED_KEYS);
            stmt.setString(1, usuario.getNome());
            stmt.setString(2, usuario.getDatanascBD());
            stmt.setString(3, usuario.getEmail());
            stmt.setString(4, usuario.getSenha());
            stmt.executeUpdate();
            
            ResultSet rs = stmt.getGeneratedKeys();  
            if(rs.next())
            	usuario.setCodU(rs.getInt(1));
            	if(!returnPassword)
            		usuario.setSenha(null);
            rs.close();
            con.commit();
            
            return usuario;
        } catch (SQLException ex) {
            try{con.rollback();}catch(SQLException ex1){System.out.println("Erro ao tentar rollback. Ex="+ex1.getMessage());};
            throw new SQLException("Erro ao inserir um usuário no banco de dados. Origem = "+ex.getMessage());
        } finally{
            try{stmt.close();}catch(Exception ex){
            	throw new Exception("Erro ao fechar stmt. Ex="+ex.getMessage());
            	};
            try{con.close();;}catch(Exception ex){
            	throw new Exception("Erro ao fechar conexão. Ex="+ex.getMessage());
            	};
        }
    }
    
    public boolean alterarUsuario(Usuario usuario) throws SQLException, Exception{
        Connection con = null;
        PreparedStatement stmt = null;
        try{
            con = ConnectionFactory.getConnection();
            stmt = con.prepareStatement(stmtAlterarU);
            stmt.setString(1, usuario.getNome());
            stmt.setString(2, usuario.getDatanasc());
            stmt.setString(3, usuario.getEmail());
            stmt.setString(4, usuario.getSenha());
            stmt.setInt(5, usuario.getCodU());
            stmt.executeUpdate();
            
            return true;
        }catch(SQLException ex){
            throw new SQLException("Erro ao alterar o usuario. Origem = " +ex.getMessage());
        }finally{
            try{stmt.close();}catch(Exception ex){
            	throw new Exception("Erro ao fechar stmt. Ex = " +ex.getMessage());
            	};
            try{con.close();;}catch(Exception ex){
            	throw new Exception("Erro ao fechar conexão. Ex= "+ex.getMessage());
            	};
        }
    }
    
    public boolean removerUsuario(Usuario usuario) throws SQLException, Exception {
        Connection con = null;
        PreparedStatement stmt = null;
        try{
            con = ConnectionFactory.getConnection();
            stmt = con.prepareStatement(stmtRemoverU);
            stmt.setInt(1, usuario.getCodU());
            stmt.executeUpdate();
            
            return true;
        }catch(SQLException ex){
            throw new SQLException("Erro ao deletar o usuario. Origem=" +ex.getMessage());
        }finally{
            try{stmt.close();}catch(Exception ex){
            	throw new Exception("Erro ao fechar stmt. Ex="+ex.getMessage());
            	};
            try{con.close();;}catch(Exception ex){
            	throw new Exception("Erro ao fechar conexão. Ex="+ex.getMessage());
            	};                
        }
    }
    
    public Usuario consultarUsuario(Usuario usuario) throws SQLException, Exception {
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try{
            con = ConnectionFactory.getConnection();
            stmt = con.prepareStatement(stmtConsultar);
            stmt.setString(1, usuario.getEmail());
            
            usuario = null;
            rs = stmt.executeQuery();
            if(rs.next())
            	usuario = new Usuario(rs.getInt("codu"), rs.getString("nome"), rs.getString("datanasc"),rs.getString("email"), null);
            return usuario;
        }catch(SQLException ex){
            throw new SQLException("Erro ao fazer login. Origem="+ex.getMessage());
        }finally{
            try{rs.close();}catch(Exception ex){
            	ex.printStackTrace();
            	throw new Exception("Erro ao fechar result set. Ex="+ex.getMessage());
            	};
            try{stmt.close();}catch(Exception ex){
            	ex.printStackTrace();
            	throw new Exception("Erro ao fechar stmt. Ex="+ex.getMessage());
            	};
            try{con.close();;}catch(Exception ex){
            	ex.printStackTrace();
            	throw new Exception("Erro ao fechar conexão. Ex="+ex.getMessage());
            	};
        }
    }
	
    public Usuario consultarUsuarioPorCod(int codU) throws SQLException, Exception {
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Usuario usuario = null;
        try{
            con = ConnectionFactory.getConnection();
            stmt = con.prepareStatement(stmtConsultarID);
            stmt.setInt(1, codU);
            
            rs = stmt.executeQuery();
            if(rs.next())
            	usuario = new Usuario(rs.getInt("codu"), rs.getString("nome"), rs.getString("datanasc"),rs.getString("email"), null);
            return usuario;
        }catch(SQLException ex){
        	ex.printStackTrace();
            throw new SQLException("Erro ao fazer login. Origem="+ex.getMessage());
        }catch(Exception e){
        	e.printStackTrace();
            throw new Exception("Erro ao fazer login. Origem="+e.getMessage());
        }finally{
            try{rs.close();}catch(Exception ex){
            	ex.printStackTrace();
            	throw new Exception("Erro ao fechar result set. Ex="+ex.getMessage());
            	};
            try{stmt.close();}catch(Exception ex){
            	ex.printStackTrace();
            	throw new Exception("Erro ao fechar stmt. Ex="+ex.getMessage());
            	};
            try{con.close();;}catch(Exception ex){
            	ex.printStackTrace();
            	throw new Exception("Erro ao fechar conexão. Ex="+ex.getMessage());
            	};
        }
    }
    
	public Usuario consultarLogin(Usuario usuario) throws Exception{
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try{
            con = ConnectionFactory.getConnection();
            stmt = con.prepareStatement(stmtLogin);
            stmt.setString(1, usuario.getEmail());
            stmt.setString(2, usuario.getSenha());
            
            usuario = null;
            rs = stmt.executeQuery();
            if(rs.next())
            	usuario = new Usuario(rs.getInt("codu"), rs.getString("nome"), rs.getString("datanasc"),rs.getString("email"), null);
            return usuario;
        } catch (SQLException ex){
            throw new SQLException("Erro ao fazer login. Origem="+ex.getMessage());
        } finally {
            try{rs.close();}catch(Exception ex){
            	throw new Exception("Erro ao fechar result set. Ex="+ex.getMessage());
            	};
            try{stmt.close();}catch(Exception ex){
            	throw new Exception("Erro ao fechar stmt. Ex="+ex.getMessage());
            	};
            try{con.close();;}catch(Exception ex){
            	throw new Exception("Erro ao fechar conexão. Ex="+ex.getMessage());
            	};
        }
    }
	
	public boolean inserirEmUsuarioHasResiduo(Usuario u, Residuo r) throws Exception {
    	Connection con = null;
    	PreparedStatement stmt = null;
    	try {
    		con = ConnectionFactory.getConnection();
            stmt = con.prepareStatement(stmtInserirFavorito);
        	stmt.setInt(1, u.getCodU());
        	stmt.setInt(2, r.getCodR());
        	stmt.executeUpdate();
            return true;
    	} catch (SQLException ex) {
    		if(ex.getMessage().startsWith("Duplicate entry")) {
    			throw new SQLException( this.msgResiduoJaFavoritado );
    		}
    		System.err.println("u:" + u.getCodU() +", r:" + r.getCodR());
    		ex.printStackTrace();
            throw new SQLException("Erro ao inserir favorito, origem: " + ex.getMessage());
        } catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Erro ao inserir favorito, origem: " + e.getMessage());
		} finally{
            try{ stmt.close(); stmt.close();  
            }catch(Exception ex){
            	ex.printStackTrace();
            	throw new Exception("Erro ao fechar stmt. Ex="+ex.getMessage());
            	};
            try{con.close();;}catch(Exception ex){
            	throw new Exception("Erro ao fechar conexão. Ex="+ex.getMessage());
            	};
        }
    }
	
	public boolean removerDeUsuarioHasResiduo(Usuario u, Residuo r) throws Exception {
    	Connection con = null;
    	PreparedStatement stmt = null;
    	try {
    		con = ConnectionFactory.getConnection();
            stmt = con.prepareStatement(stmtRemoverFavorito);
        	stmt.setInt(1, u.getCodU());
        	stmt.setInt(2, r.getCodR());
        	stmt.executeUpdate();
            return true;
    	} catch (SQLException ex) {
    		ex.printStackTrace();
            throw new SQLException("Erro ao inserir favorito, origem: " + ex.getMessage());
        } catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Erro ao inserir favorito, origem: " + e.getMessage());
		} finally{
            try{ stmt.close(); stmt.close();  
            }catch(Exception ex){
            	ex.printStackTrace();
            	throw new Exception("Erro ao fechar stmt. Ex="+ex.getMessage());
            	};
            try{con.close();;}catch(Exception ex){
            	throw new Exception("Erro ao fechar conexão. Ex="+ex.getMessage());
            	};
        }
    }
	
	public List<Residuo> listarUsuarioHasResiduo(Usuario u) throws Exception {
    	Connection con = null;
    	PreparedStatement stmt = null;
    	List<Residuo> residuos = null;
    	try {
    		con = ConnectionFactory.getConnection();
            stmt = con.prepareStatement(stmtListarFavoritos);
        	stmt.setInt(1, u.getCodU());
        	ResultSet rs = stmt.executeQuery();
        	residuos = new ArrayList<Residuo>();
        	ResiduoDAO resDAO = new ResiduoDAO();
        	while(rs.next()) {
        		int codR = rs.getInt("residuo_codr");
        		Residuo r = resDAO.pesquisarResiduo(codR);
        		if(r != null){
        			residuos.add(r);
        		}
        	}
            return residuos;
    	} catch (SQLException ex) {
    		ex.printStackTrace();
            throw new SQLException("Erro ao pesquisar favorito, origem: " + ex.getMessage());
        } catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Erro ao pesquisar favorito, origem: " + e.getMessage());
		} finally{
            try{ stmt.close(); stmt.close();  
            }catch(Exception ex){
            	ex.printStackTrace();
            	throw new Exception("Erro ao fechar stmt. Ex="+ex.getMessage());
            	};
            try{con.close();;}catch(Exception ex){
            	throw new Exception("Erro ao fechar conexão. Ex="+ex.getMessage());
            	};
        }
    }
	
	
}

package resysclagem.ws.classes;

import java.io.File;
import java.util.List;

import resysclagem.ws.PropertySource;

public class Dica {
	private int codD;
	private String nome;
	private String descricaoPath;
	private boolean isReutilizavel;
	private Residuo residuo;
	private List<String> anexos; //textos + URLs de imgs
	
	public Dica(){}
	
	public Dica(int codD, String nome, String descricaoPath, boolean isReutilizavel, Residuo residuo, List<String> anexos){
		this(codD, nome, descricaoPath, isReutilizavel);
		this.residuo = residuo;
		this.anexos = anexos;
	}

	public Dica(int codD, String nome, String descricaoPath, boolean isReutilizavel){
		this.codD = codD;
		this.nome = nome;
		setDescricaoPath(descricaoPath);
		this.isReutilizavel = isReutilizavel;
	}

	public Dica(String nome, String descricaoPath, boolean isReutilizavel, Residuo residuo){
		this.nome = nome;
		setDescricaoPath(descricaoPath);
		this.isReutilizavel = isReutilizavel;
		this.residuo = residuo;
	}

	
	public int getCodD() {
		return codD;
	}

	public void setCodD(int codD) {
		this.codD = codD;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricaoPath() {
		return descricaoPath;
	}

	public void setDescricaoPath(String descricaoPath) {
		this.descricaoPath = descricaoPath;
		setURLsToAbsolute();
	}

	public boolean getIsReutilizavel() {
		return isReutilizavel;
	}

	public void setIsReutilizavel(boolean isReutilizavel) {
		this.isReutilizavel = isReutilizavel;
	}

	public Residuo getResiduo() {
		return residuo;
	}

	public void setResiduo(Residuo residuo) {
		this.residuo = residuo;
	}

	public List<String> getAnexos() {
		return anexos;
	}

	public void setAnexos(List<String> anexos) {
		this.anexos = anexos;
	}
		
	private void setURLsToAbsolute(){
		if( (descricaoPath.contains("/") || (descricaoPath.contains(File.separator)) || (descricaoPath.isEmpty())) )
			 return;
		
		StringBuilder birl = new StringBuilder();
		birl.append(PropertySource.props.getProperty("resysclagem.launch.protocol"));
		birl.append("://");
		birl.append(PropertySource.props.getProperty("resysclagem.launch.host"));
		birl.append(":");
		birl.append(PropertySource.props.getProperty("resysclagem.launch.port"));
		birl.append(PropertySource.props.getProperty("resysclagem.file"));
		birl.append(PropertySource.props.getProperty("resysclagem.file.dica"));
		birl.append("/");
		
		this.descricaoPath = birl.toString() + descricaoPath; 
	}

}
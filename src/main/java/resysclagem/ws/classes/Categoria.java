package resysclagem.ws.classes;

public class Categoria {
	private int codC;
	private String nome;
	private String cor;		//#F1F000
	
	public Categoria (int codC, String nome, String cor){
		this(nome, cor);
		this.setCodC(codC);
	}
	
	public Categoria (String nome, String cor){
		this.setNome(nome);
		this.setCor(cor);
	}
	
	public int getCodC() {
		return codC;
	}

	public void setCodC(int codC) {
		this.codC = codC;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCor() {
		return cor;
	}
	
	public void setCor(String hex) {
		if(hex != null && !hex.isEmpty()) {
			String onlyHex = hex;
			if(hex.contains("#")){
				onlyHex = hex.substring(1);	//remove o #
			}
			if(onlyHex.matches("-?[0-9a-fA-F]+")) {
				this.cor = "#" + onlyHex;
			} else {
				//System.err.println("Categoria " +this.nome+ ": Cor não é hexadecimal! (ex. #F1F000), sendo = " + hex);
				this.cor = "#FFFFFF";
			}
		} else {
			//cor nula ou vazia
			this.cor = "#FFFFFF";
		}
	}
	
}
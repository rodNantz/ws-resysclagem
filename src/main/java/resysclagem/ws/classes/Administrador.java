package resysclagem.ws.classes;

public class Administrador {
	private int codA;
	private String nome;
	private String email;
	private String senha;
	
	public Administrador(String email, String senha) {
		super();
		this.email = email;
		this.senha = senha;
	}
	
	public Administrador(int codA, String nome, String email) {
		super();
		this.codA = codA;
		this.nome = nome;
		this.email = email;
	}

	public Administrador(int codA, String nome, String email, String senha) {
		this(codA, nome, email);
		this.senha = senha;
	}

	public int getCodA() {
		return codA;
	}

	public void setCodA(int codA) {
		this.codA = codA;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}
}

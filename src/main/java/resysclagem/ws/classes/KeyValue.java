package resysclagem.ws.classes;

public class KeyValue {
	// Abstração de TipoRelatorio
    private int key; 
    private String value;
    private String extra;
    private boolean extra2;
    
	public KeyValue(int key, String value, String extra, boolean extra2) {
		super();
		this.key = key;
		this.value = value;
		this.extra = extra;
		this.extra2 = extra2;
	}
	
	public int getKey() {
		return key;
	}
	public void setKey(int key) {
		this.key = key;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}

	public String getExtra() {
		return extra;
	}

	public void setExtra(String extra) {
		this.extra = extra;
	}

	public boolean getExtra2() {
		return extra2;
	}

	public void setExtra2(boolean extra2) {
		this.extra2 = extra2;
	}
}

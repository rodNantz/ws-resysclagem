package resysclagem.ws.classes.subcls;

import java.time.LocalTime;
import java.util.Comparator;

import org.junit.runner.manipulation.Sortable;
import org.junit.runner.manipulation.Sorter;

/**
 * subclass - DiaSemana
 * 
 * @author rodtw
 *
 */
public class DiaSemana {
	private int dia;				//de 1 (DOM) a 7 (SÁB)
	private String abertura;
	private String encerramento;
	
	public DiaSemana(int dia, String abertura, String encerramento) {
		super();
		this.dia = dia;
		this.abertura = abertura;
		this.encerramento = encerramento;
	}
	
	public int getDia() {
		return dia;
	}
	public void setDia(int dia) {
		this.dia = dia;
	}
	public String getAbertura() {
		return abertura;
	}
	public void setAbertura(String abertura) {
		this.abertura = abertura;
	}
	public String getEncerramento() {
		return encerramento;
	}
	public void setEncerramento(String encerramento) {
		this.encerramento = encerramento;
	}

}
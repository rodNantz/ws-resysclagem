package resysclagem.ws.classes.subcls;

import java.lang.reflect.Type;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;

import javassist.expr.Instanceof;
import resysclagem.ws.classes.PontoColeta;
import resysclagem.ws.classes.Residuo;

public class PCODeserializer implements JsonDeserializer<PontoColeta> {

	public PontoColeta deserialize(JsonElement json, Type arg1, JsonDeserializationContext arg2)
			throws JsonParseException {
		try {
			final JsonObject jsonObject = json.getAsJsonObject();
			
			int codPC = 0;
		    try {
		    	codPC = jsonObject.get("codPC").getAsInt();
		    }catch(Exception e){
		    	System.out.println("INFO: codPC não foi passado");
		    }
			
		    final String nome = jsonObject.get("nome").getAsString();
		    final String pathImagem = "";
		    final String endereco = jsonObject.get("endereco").getAsString();
		    final String descricao = jsonObject.get("descricao").getAsString();
		    final String telefone = jsonObject.get("telefone").getAsString();
		    final String email = jsonObject.get("email").getAsString();
		    final String latitude = jsonObject.get("latitude").getAsString();
		    final String longitude = jsonObject.get("longitude").getAsString();
		    String msgRequisicao;
		    try {
		    	msgRequisicao = jsonObject.get("msgRequisicao").getAsString();
		    } catch (Exception e){
		    	msgRequisicao = "";
		    }
		    boolean pendente;
		    try {
		    	pendente = jsonObject.get("pendente").getAsBoolean();
		    } catch (Exception e){
		    	pendente = false;
		    }
		    
		    
		    List<Residuo> residuos = null;
		    try{
		    	Type listResType = new TypeToken<List<Residuo>>(){}.getType();
				residuos = new Gson().fromJson(
											   new Gson().toJson(jsonObject.get("residuos")),
											   listResType);
		    } catch(Exception e){
		    	System.err.println( new Gson().toJson(jsonObject.get("residuos")) );
		    	e.printStackTrace();
		    	throw e;
		    }
			
		    
		    
		    String horaAtendimento = null;
			try {
				Gson gson = new Gson(); 
				HoraAtend horaObj = gson.fromJson(
						 						  new Gson().toJson(jsonObject.get("horaAtendimento")), 
												  HoraAtend.class);
				return new PontoColeta(codPC, nome, pathImagem, endereco, descricao, telefone, 
									   email, horaObj, latitude, longitude, residuos, msgRequisicao, pendente);
			} catch(Exception e){
				//System.out.println(e.getMessage());
				horaAtendimento = jsonObject.get("horaAtendimento").getAsString();
				System.out.println("INFO: utilizado construtor de horaAtendimento como String"); 
			}
		    
		    return new PontoColeta(codPC, nome, pathImagem, endereco, descricao, telefone, 
		    					   email, horaAtendimento, latitude, longitude, residuos, msgRequisicao, pendente);

		} catch (Exception e){
			e.printStackTrace();
			return null;
		}
	    
	}
	

}

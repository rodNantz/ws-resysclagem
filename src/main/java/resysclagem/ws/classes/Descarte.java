package resysclagem.ws.classes;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

public class Descarte {
	private int codD;
	private Usuario usuario;
	private PontoColeta pontoColeta;
	private String dataD;
	private boolean statusD;
	private List<Residuo> residuos;
	
	public Descarte (int codD, Usuario usuario, PontoColeta pontoColeta, String dataD, boolean statusD, List<Residuo> residuos) {
		this.codD = codD;
		this.usuario = usuario;
		this.pontoColeta = pontoColeta;
		this.dataD = dataD;
		this.statusD = statusD;
		this.residuos = residuos;
	}

	public int getCodD() {
		return codD;
	}

	public void setCodD(int codD) {
		this.codD = codD;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
	public PontoColeta getPontoColeta() {
		return pontoColeta;
	}

	public void setPontoColeta(PontoColeta pontoColeta) {
		this.pontoColeta = pontoColeta;
	}

	public String getDataD() {
		return dataD;
	}

	public String getDataDB() {
		try {
			SimpleDateFormat brFormat = new SimpleDateFormat("dd/MM/yyyy");
			SimpleDateFormat dbFormat = new SimpleDateFormat("yyyy-MM-dd");
			if(this.dataD.contains("/")){
				return dbFormat.format(brFormat.parse(this.dataD));
			} else {
				return this.dataD;
			}
		} catch (ParseException e) {
		    e.printStackTrace();
		    return dataD;
		}
	}
	
	public void setDataD(String dataD) {
		this.dataD = dataD;
	}

	public boolean isStatusD() {
		return statusD;
	}

	public void setStatusD(boolean statusD) {
		this.statusD = statusD;
	}

	public List<Residuo> getResiduos() {
		return residuos;
	}

	public void setResiduos(List<Residuo> residuos) {
		this.residuos = residuos;
	}
	
}

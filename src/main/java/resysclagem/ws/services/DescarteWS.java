package resysclagem.ws.services;


import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;

import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


import com.google.gson.Gson;

import resysclagem.ws.classes.Descarte;

import resysclagem.ws.classes.MsgResponse;

import resysclagem.ws.classes.Request;
import resysclagem.ws.classes.Usuario;
import resysclagem.ws.dao.DescarteDAO;
import resysclagem.ws.launch.JettyLauncher;

/**
 * REST endpoint para Descarte.
 * 
 * 
 * @version 18/05/2017
 * @author TeamTCC
 *
 */
@Path("/descarte")
public class DescarteWS {

	Gson gson = new Gson();
	private DescarteDAO desDAO; 

	//create
	/**
	 * Descarte só pode ser feito por usuário
	 * 
	 * @param strDescarte
	 * @return
	 */
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response novoDescarte (String strRequest){
		try {
			gson = new Gson();
			Request request = gson.fromJson(strRequest, Request.class);
			// checar token
			if(!JettyLauncher.tokenClass.isUsuTokenValido(request)) {
				return Response.ok().entity( 
						new MsgResponse("request", false, "Token inválido!")
						).build();
			}
			Descarte des = gson.fromJson(request.getObjeto(), Descarte.class);
			desDAO = new DescarteDAO();
			boolean condicao = true; 	//caso a gente queira implementar alguma precondição
			if(!condicao){
				return Response.ok().entity(
						new MsgResponse("inserir", false, "Não foi possível descartar!")
						).build();
			}
			boolean inseriu = desDAO.inserirDescarte(des);
			if (inseriu){
				return Response.status(201)
						.entity(new MsgResponse("inserir", true, "Descarte feito com sucesso!"))
						.build();
			} else {
				return Response.status(201)
						.entity(new MsgResponse("inserir", false, "Descarte não foi realizado!"))
						.build();
			}
		} catch(SQLException sqle){
			sqle.printStackTrace();
			return Response.status(500)
					.entity(new MsgResponse("inserir", false, "Erro interno: " + sqle.getMessage()))
					.build();
		}catch (Exception e) {
			e.printStackTrace();
			return Response.status(500)
					.entity(new MsgResponse("inserir", false, "Erro interno: " + e.getMessage()))
					.build();
		}
	}
	
	//read
	@POST
	@Path("/lista")
    @Produces(MediaType.APPLICATION_JSON)
    public Response listarDescartes(String strRequest) throws SQLException, ClassNotFoundException {
		try{
			gson = new Gson();
			Request request = gson.fromJson(strRequest, Request.class);
			// checar token
			if(!JettyLauncher.tokenClass.isUsuTokenValido(request)) {
				return Response.ok().entity( 
						new MsgResponse("request", false, "Token inválido!")
						).build();
			}
			desDAO = new DescarteDAO();
			Usuario usuario = gson.fromJson(request.getObjeto(), Usuario.class);
        	List<Descarte> descartes = desDAO.listarDescartes(usuario);
	        if (!descartes.isEmpty()){
	        	return Response.ok().entity(
	        			gson.toJson(descartes)
	        			).build();			
	        } else {
	        	return Response.ok().entity(
	        			new ArrayList<Descarte>()
	        			).build();
	        }
        } catch (SQLException sqle){
        	sqle.printStackTrace();
        	return Response.serverError().entity(
        			sqle.getMessage()
        			).build();
        } catch (Exception e){
        	e.printStackTrace();
        	return Response.serverError().entity(
        			e.getMessage()
        			).build();
        }   
        
	}
	
	//read
	/**
	 * 
	 * 
	 * @param strRequest
	 * @return Response
	 * @author rodrigo.nantes@outlook.com
	 */
	@POST
	@Path("/dousuario")
	@Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response buscarDescartesDoUsuario(String strRequest) {
		try {
			gson = new Gson();
			Request request = gson.fromJson(strRequest, Request.class);
			// checar token
			if(!JettyLauncher.tokenClass.isUsuTokenValido(request)) {
				return Response.ok().entity( 
						new MsgResponse("request", false, "Token inválido!")
						).build();
			}
			Usuario usuario = gson.fromJson(request.getObjeto(), Usuario.class);
			desDAO = new DescarteDAO();
			
			List<Descarte> lista = desDAO.buscarDescartesDoUsuario(usuario);
			if(lista.isEmpty()){
	        	return Response.ok().entity(
	        			new MsgResponse("busca", true, "Nenhum ponto de coleta com este resíduo encontrado!")
	        			).build();
			} else {
				return Response.ok().entity(
	        			gson.toJson(lista)
	        			).build();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return Response.serverError().entity(
	    			new MsgResponse("busca", false, "Erro do servidor: " + e.getMessage())
	    			).build();
		}
	}
		
	//update
	/**
	 * A ideia aqui é receber um descarte existente e atualizá-lo (geralmente, para confirmar descarte - mudar status para true).
	 * 
	 * @param strPonto
	 * @return
	 */
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response atualizarDescarte (String strRequest){
		try{
			gson = new Gson();
			Request request = gson.fromJson(strRequest, Request.class);
			// checar token
			if(!JettyLauncher.tokenClass.isUsuTokenValido(request)) {
				return Response.ok().entity( 
						new MsgResponse("request", false, "Token inválido!")
						).build();
			}
			Descarte descarte = gson.fromJson(request.getObjeto(), Descarte.class);
			desDAO = new DescarteDAO();
			if(desDAO.alterarDescarte(descarte)) {
				return Response.ok().entity( 
						new MsgResponse("update", true, "Descarte atualizado!")
						).build();
			}
			return Response.ok().entity( 
					new MsgResponse("update", false, "Descarte não atualizado!")
					).build();
		} catch(Exception e){
			e.printStackTrace();
			return Response.status(500).entity(
					new MsgResponse("update", false, e.getMessage())
					).build();
		}
		
	}
	
	//delete
	@DELETE
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response deletarDescarte (Response strDescarte) {
		//TODO
		return Response.ok(
				new MsgResponse("delete", false, "Ainda não implementado!")
				).build();
	}
		
}

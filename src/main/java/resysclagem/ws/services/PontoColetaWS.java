package resysclagem.ws.services;

import java.io.File;
import java.lang.reflect.Type;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.io.FilenameUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import resysclagem.ws.classes.Dica;
import resysclagem.ws.classes.FilePath;
import resysclagem.ws.classes.MsgResponse;
import resysclagem.ws.classes.PontoColeta;
import resysclagem.ws.classes.Request;
import resysclagem.ws.classes.Residuo;
import resysclagem.ws.classes.subcls.PCODeserializer;
import resysclagem.ws.classes.FilePath.AppendEnum;
import resysclagem.ws.classes.FilePath.PathType;
import resysclagem.ws.dao.DicaDAO;
import resysclagem.ws.dao.PontoColetaDAO;
import resysclagem.ws.dao.ResiduoDAO;
import resysclagem.ws.launch.JettyLauncher;
import resysclagem.ws.utilities.DicaHtmlBuilder;

/**
 * REST endpoint para ponto de coleta.
 * 
 * 
 * @version 01/05/2017
 * @author TeamTCC
 *
 */
@Path("/pontocoleta")
public class PontoColetaWS {

	Gson gson = new Gson();
	private PontoColetaDAO pcoDAO; 

	//create
	/**
	 * Cadastro de novo ponto de coleta, pendente ou já aprovado. 
	 * Deve ser pendente pelo usuário e aprovado pelo admin.
	 * 
	 * @param strRequest
	 * @return
	 */
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response novoPontoColeta (String strRequest){
		Request request = gson.fromJson(strRequest, Request.class);
		// checar token
		if(!JettyLauncher.tokenClass.isTokenValido(request)) {
			return Response.ok().entity( 
					new MsgResponse("request", false, "Token inválido!")
					).build();
		}
		//pra usar o construtor certo
		GsonBuilder gsonBuilder = new GsonBuilder();
	    gsonBuilder.registerTypeAdapter(PontoColeta.class, new PCODeserializer());
	    Gson cGson = gsonBuilder.create();
		
		PontoColeta pco = cGson.fromJson(request.getObjeto(), PontoColeta.class);
		pcoDAO = new PontoColetaDAO();
		try {
			List<PontoColeta> listPCO = pcoDAO.buscaExataNomePCO(pco);
			if(!listPCO.isEmpty()){
				return Response.ok().entity(
						new MsgResponse("inserir", false, "Já existe um ponto com esse mesmo nome! Tente outro")
						).build();
			}
		
			FilePath fp = new FilePath(PathType.SERVICE, AppendEnum.PONTOCOLETA);
			pco.setImagem(
					fp.getFullPath()
					+ "/" + pco.getCodPC()
					+ "/" + FilenameUtils.getExtension(pco.getImagem())
					);
			PontoColeta pontoInserido = pcoDAO.inserirPCO(pco);
			if (pontoInserido != null){
				return Response.status(201)
						.entity(new MsgResponse("create", true, 
												"Ponto de coleta inserido com sucesso!",
												gson.toJson(pontoInserido)))
						.build();
			}
		} catch(SQLException sqle){
			sqle.printStackTrace();
			return Response.status(500)
					.entity(new MsgResponse("create", false, "Erro interno: " + sqle.getMessage()))
					.build();
		}catch (Exception e) {
			e.printStackTrace();
			return Response.status(500)
					.entity(new MsgResponse("create", false, "Erro interno: " + e.getMessage()))
					.build();
		}
		return null;
	}
	
	//read
	@GET
	@Path("/lista")
    @Produces(MediaType.APPLICATION_JSON)
    public Response listaPCOs() throws SQLException, ClassNotFoundException {
    
		pcoDAO = new PontoColetaDAO();
		
        try{
        	List<PontoColeta> pontos = pcoDAO.listarTodosPCOs(false);
	        if (!pontos.isEmpty()){
	        	return Response.ok().entity(
	        			gson.toJson(pontos)
	        			).build();			
	        } else {
	        	return Response.ok().entity(
	        			new ArrayList<PontoColeta>()
	        			).build();
	        }
        } catch (SQLException sqle){
        	sqle.printStackTrace();
        	return Response.serverError().entity(
        			sqle.getMessage()
        			).build();
        } catch (Exception e){
        	e.printStackTrace();
        	return Response.serverError().entity(e.getMessage()).build();
        }   
        
	}
	
	//read
	@GET
	@Path("/pendente")
    @Produces(MediaType.APPLICATION_JSON)
    public Response listaPCOsPendentes() throws SQLException, ClassNotFoundException {
    
		pcoDAO = new PontoColetaDAO();
		
        try{
        	List<PontoColeta> pontos = pcoDAO.listarTodosPCOs(true);
	        if (!pontos.isEmpty()){
	        	return Response.ok().entity(
	        			gson.toJson(pontos)
	        			).build();			
	        } else {
	        	return Response.ok().entity(
	        			new ArrayList<PontoColeta>()
	        			).build();
	        }
        } catch (SQLException sqle){
        	sqle.printStackTrace();
        	return Response.serverError().entity(
        			sqle.getMessage()
        			).build();
        } catch (Exception e){
        	e.printStackTrace();
        	return Response.serverError().entity(e.getMessage()).build();
        }   
        
	}
	
	//read
	/**
	 * <pre>
	 * WS que recebe uma lista de resíduos e retorna os pontos de coleta que
	 * aceitam esses resíduos. Por enquanto, admin e usuário podem usar.
	 * </pre>
	 * 
	 * @param strResiduo
	 * @return Response
	 * @author rodrigo.nantes@outlook.com
	 */
	@POST
	@Path("/comresiduo")
	@Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response buscaPCOsComResiduos(String strRequest) {
		Request request = gson.fromJson(strRequest, Request.class);
		// checar token
		if(!JettyLauncher.tokenClass.isTokenValido(request)) {
			return Response.ok().entity( 
					new MsgResponse("request", false, "Token inválido!")
					).build();
		}
		Type listResType = new TypeToken<List<Residuo>>() {}.getType();
		List<Residuo> residuos = gson.fromJson(request.getObjeto(), listResType);
		pcoDAO = new PontoColetaDAO();
		try {
			List<PontoColeta> listPCOs = pcoDAO.buscarPCOsQueTenhamResiduos(residuos);
			if(listPCOs.isEmpty()) {
	        	return Response.ok().entity(
	        			new ArrayList<PontoColeta>()
	        			).build();
			} else {
				return Response.ok().entity(
	        			gson.toJson(listPCOs)
	        			).build();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return Response.serverError().entity(
					new ArrayList<MsgResponse>().add(
						new MsgResponse("busca", false, "Erro do servidor: " + e.getMessage())
						)
	    			).build();
		}
	}
		
	//update
	/**
	 * Atuaizar pontos de coleta. Usado pelo admin.
	 * 
	 * @param strRequest
	 * @return
	 */
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response atualizarPCO (String strRequest) {
		Request request = gson.fromJson(strRequest, Request.class);
		// checar token
		if(!JettyLauncher.tokenClass.isAdminTokenValido(request)) {
			return Response.ok().entity( 
					new MsgResponse("request", false, "Token inválido!")
					).build();
		}
		try {
			//pra usar o construtor certo
			GsonBuilder gsonBuilder = new GsonBuilder();
		    gsonBuilder.registerTypeAdapter(PontoColeta.class, new PCODeserializer());
		    Gson cGson = gsonBuilder.create();
			
			PontoColeta pco = cGson.fromJson(request.getObjeto(), PontoColeta.class);
			pcoDAO = new PontoColetaDAO();
			
			FilePath fp = new FilePath(PathType.SERVICE, AppendEnum.PONTOCOLETA);
			pco.setImagem(
					fp.getFullPath()
					+ "/" + pco.getCodPC()
					+ "/" + FilenameUtils.getExtension(pco.getImagem())
					);
			PontoColeta pontoAlterado = pcoDAO.alterarPCO(pco);
			if (pontoAlterado != null){
				return Response.status(201)
						.entity(new MsgResponse("create", true, 
												"Ponto de coleta alterado com sucesso!",
												gson.toJson(pontoAlterado)))
						.build();
			}
		} catch(SQLException sqle){
			sqle.printStackTrace();
			return Response.status(500)
					.entity(new MsgResponse("create", false, "Erro interno: " + sqle.getMessage()))
					.build();
		}catch (Exception e) {
			e.printStackTrace();
			return Response.status(500)
					.entity(new MsgResponse("create", false, "Erro interno: " + e.getMessage()))
					.build();
		}
		return null;
	}
	
         
	//delete
	/**
	 * Apeans Admin deleta.
	 * 
	 * @param strRequest
	 * @return
	 */
	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	public Response deletarPCO (@QueryParam("cod") Integer codDica,
			 					@QueryParam("id") String emailAdmin,
			 					@QueryParam("t") String token) {
		//
		try {
			Request request = new Request(codDica.toString(), emailAdmin, token);
			// checar token
			if(!JettyLauncher.tokenClass.isAdminTokenValido(request)) {
				return Response.ok().entity( 
						new MsgResponse("request", false, "Token inválido!")
						).build();
			}
			pcoDAO = new PontoColetaDAO();
			PontoColeta pco = new PontoColeta(codDica, "", "", "", "", "", "", "", "0", "0", 
											  new ArrayList<Residuo>(), 
											  "", false);
			
	    	boolean deletou = pcoDAO.removerPCO(pco);
	        if (deletou){
	        	return Response.ok().entity(
	        			new MsgResponse("delete", true, "Ponto removido!")
	        			).build();			
	        } else {
	        	return Response.ok().entity(
	        			new MsgResponse("delete", false, "Não pôde remover ponto!")
	        			).build();
	        }
		} catch (Exception e) {
			e.printStackTrace();
			return Response.status(500).build();
		}
	}
		
}

package resysclagem.ws.services;

import java.lang.reflect.Type;
import java.sql.SQLException;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import resysclagem.ws.Enums;
import resysclagem.ws.classes.Administrador;
import resysclagem.ws.classes.MsgResponse;
import resysclagem.ws.classes.Request;
import resysclagem.ws.classes.Residuo;
import resysclagem.ws.classes.Usuario;
import resysclagem.ws.dao.UsuarioDAO;
import resysclagem.ws.launch.JettyLauncher;
import resysclagem.ws.utilities.Utility;

@Path("/usuario")
public class UsuarioWS {

	/*
	 * LEMBRETES / TODO:
	 * - Receber senha somente em SHA256 (64 caracteres)
	 * - Retornar um token ao Android, para ser usado durante a sessão (?)
	 */
	
    @Context
    private UriInfo context;
    
	Gson gson = new Gson();
	private UsuarioDAO usuarioDAO;
	
	
	@GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response usuarioExiste (@QueryParam("email") String email) throws SQLException, ClassNotFoundException {
		usuarioDAO = new UsuarioDAO();
		try {
			if(email != null){
				Usuario usuario = usuarioDAO.consultarUsuario(new Usuario(email));
				Request request = new Request(gson.toJson(usuario), "", "");
				if (usuario != null){
					return Response.ok()
							.entity(new ContaUsuario(true, false, request))
							.build();	
				} else {
					return Response.ok()
							.entity(new ContaUsuario(false, false, null))
							.build();
				}
			} else {
				return Response.status(412).build();	//email null
			}
		} catch (Exception e) {
			e.printStackTrace();
			return Response.ok()
					.entity(new MsgResponse("consulta", false, "Erro interno: " + e.getMessage()))
					.build();
		}
    }
	
	/**
	 * Pode ser criado por qualquer não-usuário
	 * 
	 * @param strRequest
	 * @return
	 */
	@POST
	@Path("/cadastro")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response cadastroDeUsuario(String strUsuario){		
		usuarioDAO = new UsuarioDAO();
		try {
			Usuario usuario = gson.fromJson(strUsuario,
											Usuario.class);
			Usuario usuarioExiste = usuarioDAO.consultarUsuario(usuario);
			//se o usuário já existir
			if(usuarioExiste != null) {
				return Response.ok()
						.entity(new MsgResponse("cadastro", false, "Parece que esse usuário já existe! Porque não tenta fazer Login?"))
						.build();
			}
			Usuario created = usuarioDAO.inserirUsuario(usuario, true);
			if(created != null){
				// inciar session - obj ContaUsuario as string
				Response loginResponse = this.login(gson.toJson(created));
				return Response.ok( new MsgResponse("cadastro", true, "Usuário cadastrado com sucesso!", 
													loginResponse.getEntity().toString() ) 
						).build();
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return Response.status(500)
							.entity(new MsgResponse("cadastro", false, e.getMessage()))
							.build();
		} catch (Exception e) {
			e.printStackTrace();
			return Response.status(500)
							.entity(new MsgResponse("cadastro", false, e.getMessage()))
							.build();
		}
		return Response.status(500).entity(new MsgResponse("cadastro", false, "Bad server, erro no sistema!")).build();
	}
	
	
	/**
	 * Método de login de usuário.
	 * 
	 * @param strUsuario
	 * @return
	 */
	@POST
    @Path("/session")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response login(String strUsuario) {		
		Usuario usuarioReq = gson.fromJson(strUsuario, Usuario.class);        
        boolean credential = false;
        try {
        	usuarioDAO = new UsuarioDAO();
            Usuario usuarioExiste = usuarioDAO.consultarUsuario(usuarioReq);
            Request request = new Request(gson.toJson(usuarioExiste), "", "");
        	if(usuarioExiste != null){
        		credential = checkCredentials(usuarioReq, usuarioDAO);
        	} else {
        		//usuário não existe
            	return Response.ok()
            			.entity(gson.toJson(
            					new ContaUsuario(false, false, request)))
            			.header("Access-Control-Allow-Origin", "*")
                        .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT")
                        .allow("OPTIONS")
            			.build();
        	}
        	if (credential){
        		request.setId(usuarioExiste.getEmail());
	            request.setToken( JettyLauncher.tokenClass.getUsuToken(usuarioExiste.getEmail()) );
        	}
        	return Response.ok()
            		.entity(gson.toJson(
            				new ContaUsuario(true, credential, request)))
            		.build();
            
		} catch (Exception e) {
			e.printStackTrace();
		}
        
        return Response.status(500)
        		.entity("Bad, bad server!")
        		.build();
	}
	
	private boolean checkCredentials(Usuario usuarioReq, UsuarioDAO usuDAO) 
					throws SQLException, ClassNotFoundException, Exception {
	    boolean result = false;
	    if(Utility.isNotNull(usuarioReq.getEmail()) && Utility.isNotNull(usuarioReq.getSenha())){
	      try {
	    	  Usuario usu = usuarioDAO.consultarLogin(usuarioReq);
	    	  if(usu != null){
	    		  result = true;
	    	  }
	      } catch (SQLException e) {
	          throw e;
	      } catch (ClassNotFoundException e) {
	          System.out.println("Erro: " + e);
	          throw e;
	      } catch (Exception e) {
	          System.out.println("Erro: " + e);
	          throw e;
	      }
	    }else{
	        result = false;
	    }
	    return result;
	}
	
	/*
	 * DELETE into /session : logout
	 */
	@PUT
	
	@Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateUsuario(String strRequest) {
		try{
			Request request = gson.fromJson(strRequest, Request.class);
			// checar token, com condição adicional - o próprio usuário deve alterar
			if(!JettyLauncher.tokenClass.isUsuTokenValido(request)) {
				return Response.ok().entity( 
						new MsgResponse("request", false, "Token inválido!")
						).build();
			}
			Type listUsuType = new TypeToken<List<Usuario>>(){}.getType();
			
			List<Usuario> usuarios = gson.fromJson(request.getObjeto(), listUsuType);
			if(!request.getId().equals(usuarios.get(0).getEmail())){
				return Response.ok().entity( 
						new MsgResponse("Alteração", false, "Usuário inválido!")
						).build();
			}
			usuarioDAO = new UsuarioDAO();
			boolean alterou = usuarioDAO.alterarUsuario(usuarios.get(1));
			if(alterou) {
				return Response.status(200)
							   .entity(new MsgResponse("Alteração", true, "Usuário alterado!"))
							   .build();
			} else {
				return Response.status(200)
						   .entity(new MsgResponse("Alteração", false, "Usuário não alterado!"))
						   .build();
			}
		} catch (SQLException sqle){
			return Response.status(500)
					   .entity(new MsgResponse("Alteração", false, sqle.getMessage()))
					   .build();
		} catch (Exception e){
			return Response.status(500)
					   .entity(new MsgResponse("Alteração", false, e.getMessage()))
					   .build();
		}
	}
	
	/*
	 * DELETE into /session : logout
	 */
	@DELETE
    @Path("/session") ///{email}/{senha}
	@Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteSession(Usuario usuario) throws SQLException, ClassNotFoundException{
		// TO DO
		return Response.status(418).build();
	}
	
	@POST
	@Path("/favoritar")
	@Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response favoritarResiduo(String strRequest) throws SQLException, ClassNotFoundException{
		try{
			gson = new Gson();
			Request request = gson.fromJson(strRequest, Request.class);
			// checar token
			if(!JettyLauncher.tokenClass.isUsuTokenValido(request)) {
				return Response.ok().entity( 
						new MsgResponse("request", false, "Token inválido!")
						).build();
			}
			usuarioDAO = new UsuarioDAO();
			UsuarioResiduo usuRes = gson.fromJson(request.getObjeto(), UsuarioResiduo.class);
			boolean inseriu = usuarioDAO.inserirEmUsuarioHasResiduo(usuRes.usuario, usuRes.residuo);
			if(inseriu) {
				return Response.status(201)
							   .entity(new MsgResponse("favoritar", true, "Resíduo favoritado!"))
							   .build();
			} else {
				return Response.status(200)
						   .entity(new MsgResponse("favoritar", false, "Resíduo não favoritado!"))
						   .build();
			}
		} catch (SQLException sqle){
			if (sqle.getMessage().equals(usuarioDAO.msgResiduoJaFavoritado)) {
				return Response.status(200)
						   .entity(new MsgResponse("favoritar", false, usuarioDAO.msgResiduoJaFavoritado)) 
						   .build();
	    	}
			return Response.status(500)
					   .entity(new MsgResponse("favoritar", false, sqle.getMessage()))
					   .build();
		} catch (Exception e){
			return Response.status(500)
					   .entity(new MsgResponse("favoritar", false, e.getMessage()))
					   .build();
		}
		
	}
	
	//TODO
	@POST
	@Path("/desfavoritar")
	@Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response desfavoritarResiduo(String strRequest) throws SQLException, ClassNotFoundException{
		try{
			Request request = gson.fromJson(strRequest, Request.class);
			// checar token
			if(!JettyLauncher.tokenClass.isUsuTokenValido(request)) {
				return Response.ok().entity( 
						new MsgResponse("request", false, "Token inválido!")
						).build();
			}
			UsuarioResiduo usuRes = gson.fromJson(request.getObjeto(), UsuarioResiduo.class);
			usuarioDAO = new UsuarioDAO();
			boolean removeu = usuarioDAO.removerDeUsuarioHasResiduo(usuRes.usuario, usuRes.residuo);
			if(removeu) {
				return Response.status(201)
							   .entity(new MsgResponse("desfavoritar", true, "Resíduo desfavoritado!"))
							   .build();
			} else {
				return Response.status(200)
						   .entity(new MsgResponse("desfavoritar", false, "Resíduo não desfavoritado!"))
						   .build();
			}
		} catch (SQLException sqle){
			return Response.status(500)
					   .entity(new MsgResponse("desfavoritar", false, sqle.getMessage()))
					   .build();
		} catch (Exception e){
			return Response.status(500)
					   .entity(new MsgResponse("desfavoritar", false, e.getMessage()))
					   .build();
		}
		
	}
	
	@POST
	@Path("/favoritos")
	@Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response buscarResiduosFavoritos(String strRequest) throws SQLException, ClassNotFoundException{
		try {
			gson = new Gson();
			Request request = gson.fromJson(strRequest, Request.class);
			// checar token
			if(!JettyLauncher.tokenClass.isUsuTokenValido(request)) {
				return Response.ok().entity( 
						new MsgResponse("request", false, "Token inválido!")
						).build();
			}
			usuarioDAO = new UsuarioDAO();
			Usuario usuario = gson.fromJson(request.getObjeto(), Usuario.class);
			List<Residuo> residuos = usuarioDAO.listarUsuarioHasResiduo(usuario);
			if(residuos != null){
				return Response.status(200)
						   .entity(gson.toJson(residuos))
						   .build();
			} else {
				return Response.status(200)
						   .entity(new MsgResponse("favoritos", false, "Erro ao listar resíduos favoritos!"))
						   .build();
			}
		} catch (SQLException sqle){
			return Response.status(500)
					   .entity(new MsgResponse("favoritos", false, sqle.getMessage()))
					   .build();
		} catch (Exception e){
			return Response.status(500)
					   .entity(new MsgResponse("favoritos", false, e.getMessage()))
					   .build();
		}
	}
	
	@OPTIONS
	@Path("*")
	public Response options() {
	   return Response.ok("")
	   .header("Access-Control-Allow-Origin", "*")
	   .header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
	   .header("Access-Control-Allow-Credentials", "true")
	   .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
	   .header("Access-Control-Max-Age", "1209600")
	   .build();
	}
	
	
	//classes
	public class ContaUsuario {
		private boolean loginExists;
		private boolean passwordConfirmed;
		private Request request;
		
		public ContaUsuario(boolean loginExists, boolean passwordConfirmed, Request request){
			this.loginExists = loginExists;
			this.passwordConfirmed = passwordConfirmed;
			this.request = request;
		}
		
		public boolean getLoginExists() {
			return loginExists;
		}
	
		public void setLoginExists(boolean loginExists) {
			this.loginExists = loginExists;
		}
	
		public boolean getPasswordConfirmed() {
			return passwordConfirmed;
		}
	
		public void setPasswordConfirmed(boolean passwordConfirmed) {
			this.passwordConfirmed = passwordConfirmed;
		}
		
		public Request getRequest() {
			return request;
		}
	
		public void setRequest(Request request) {
			this.request = request;
		}
	}
	
	public class UsuarioResiduo {
		private Usuario usuario;
		private Residuo residuo;
		
		public UsuarioResiduo(Usuario usuario, Residuo residuo){
			this.setUsuario(usuario);
			this.setResiduo(residuo);
		}

		public Usuario getUsuario() {
			return usuario;
		}

		public void setUsuario(Usuario usuario) {
			this.usuario = usuario;
		}

		public Residuo getResiduo() {
			return residuo;
		}

		public void setResiduo(Residuo residuo) {
			this.residuo = residuo;
		}
		
	}
	
	
	
}
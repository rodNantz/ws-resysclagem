package resysclagem.ws.services;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.google.gson.Gson;

import resysclagem.ws.classes.Categoria;
import resysclagem.ws.classes.FilePath;
import resysclagem.ws.classes.MsgResponse;
import resysclagem.ws.classes.Request;
import resysclagem.ws.classes.Residuo;
import resysclagem.ws.dao.ResiduoDAO;
import resysclagem.ws.dao.UsuarioDAO;
import resysclagem.ws.launch.JettyLauncher;


/**
 * REST endpoint para resíduos. O ideal é que só seja chamado 
 * quando há mudanças no banco, e que os resíduos sejam guardados 
 * localmente no Android em cache.
 * 
 * @author TeamTCC
 *
 */
@Path("/residuo")
public class ResiduoWS {

	Gson gson = new Gson();
	private ResiduoDAO residuoDAO;
	
	//create
	/**
	 * Inserir novo resíduo no sistema. Apenas admin.
	 * 
	 * @param strResiduo
	 * @return
	 */
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response inserirResiduo(String strRequest){
		try {
			Request request = gson.fromJson(strRequest, Request.class);
			// checar token
			if(!JettyLauncher.tokenClass.isAdminTokenValido(request)) {
				return Response.ok().entity( 
						new MsgResponse("request", false, "Token inválido!")
						).build();
			}
			Residuo residuo = gson.fromJson(request.getObjeto(), Residuo.class);
			residuoDAO = new ResiduoDAO();
			List<Residuo> residuoExiste = residuoDAO.buscaExataResiduo(residuo);
			
			//se resíduo com mesmo nome já existir
			if(!residuoExiste.isEmpty()){
				return Response.status(201)
						.entity(new MsgResponse("cadastro", false, "Existe um resíduo de mesmo nome, tente um nome diferente!"))
						.build();
			}
			Residuo created = residuoDAO.inserirResiduo(residuo);
			if(residuo != null){
				return Response.ok( new MsgResponse("cadastro", true, "Resíduo inserido!", gson.toJson(created)) )
							   .build();
			}
		} catch (SQLException e) {
			return Response.status(500).entity(new MsgResponse("cadastro", false, "500: Erro no sistema! \n" + e.getMessage())).build();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return Response.status(500).entity(new MsgResponse("cadastro", false, "500: Erro no sistema!")).build();
	}
	
	//read
	@GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response buscaResiduo(@QueryParam("busca") String busca, @QueryParam("cod") String cod) {  
		residuoDAO = new ResiduoDAO();
		List<Residuo> residuos = new ArrayList<Residuo>();
        try{
        	if(cod != null){
        		Residuo residuo = residuoDAO.pesquisarResiduo(Integer.valueOf(cod));
        		return Response.ok().entity(
	        			gson.toJson(residuos.add(residuo))
	        			).build();
        	} else if (busca != null) {
        		residuos = residuoDAO.buscarResiduo(new Residuo(busca, "", ""));
	        	return Response.ok().entity(
	        			gson.toJson(residuos)
	        			).build();
        	} else {
        		return Response.status(412).build();
        	}
        } catch (SQLException e){
        	return Response.serverError().entity(
        			e.getMessage()
        			).build();
        } catch (Exception e){
        	return Response.serverError().build();
        }   
    }
	
	@GET
	@Path("/lista")
    @Produces(MediaType.APPLICATION_JSON)
    public Response listaResiduos() throws SQLException, ClassNotFoundException {
        
		residuoDAO = new ResiduoDAO();
		
        try{
        	List<Residuo> residuos = residuoDAO.listarResiduos();
        	return Response.ok().entity(
        			gson.toJson(residuos)
        			).build();			
        } catch (SQLException e){
        	return Response.serverError().entity(
        			e.getMessage()
        			).build();
        } catch (Exception e){
        	return Response.serverError().build();
        }   
        
	}
	
	/**
	 * Buscar resíduos de dada categoria. Acessado por admin ou usuário.
	 * 
	 * @param strCategoria
	 * @return
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 */
	@POST
	@Path("/dacategoria")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
    public Response buscaResiduosDeCategoria(String strRequest) throws SQLException, ClassNotFoundException {
		Request request = gson.fromJson(strRequest, Request.class);
		// checar token
		if(!JettyLauncher.tokenClass.isTokenValido(request)) {
			return Response.ok().entity( 
					new MsgResponse("request", false, "Token inválido!")
					).build();
		}
		residuoDAO = new ResiduoDAO();
		Categoria categoria = gson.fromJson(request.getObjeto(), Categoria.class); 
        try{
        	List<Residuo> residuos = residuoDAO.listarResiduos(categoria);
	        if(!residuos.isEmpty()){
	        	return Response.ok().entity(
	        			gson.toJson(residuos)
	        			).build();			
	        } else {
	        	return Response.ok().entity(
	        			"{}"
	        			).build();
	        }
        } catch (SQLException e){
        	return Response.serverError().entity(
        			e.getMessage()
        			).build();
        } catch (Exception e){
        	return Response.serverError().build();
        }   
        
	}
	
	//update
	/**
	 * Update do ponto de coleta. Admin only
	 * 
	 * @param strRequest
	 * @return
	 */
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response alterar (String strRequest){		
		try {
			Request request = gson.fromJson(strRequest, Request.class);
			// checar token
			if(!JettyLauncher.tokenClass.isAdminTokenValido(request)) {
				return Response.ok().entity( 
						new MsgResponse("request", false, "Token inválido!")
						).build();
			}
			Residuo residuo = gson.fromJson(request.getObjeto(), Residuo.class);
			residuoDAO = new ResiduoDAO();
			Residuo changed = residuoDAO.alterarResiduo(residuo);
			if(changed != null){
				return Response
						.ok()
						.entity(new MsgResponse("alterar", true, "Resíduo alterado!", gson.toJson(changed)))
						.build();
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return Response
					.status(200)
					.entity( new MsgResponse("alterar", false, "Erro: " + e.getMessage()) )
					.build();
		} catch (Exception e) {
			Request request = gson.fromJson(strRequest, Request.class);
			System.err.println(request.getObjeto());
			e.printStackTrace();
		}
		return Response
				.status(500)
				.entity(
						gson.toJson(new MsgResponse("alterar", false, "Erro no sistema!"))
				).build();
	}
	
	//delete
	/**
	 * 
	 * 
	 * @param strRequest
	 * @return
	 */
	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	public Response remover (@QueryParam("cod") Integer codR,
							 @QueryParam("id") String admEmail,
							 @QueryParam("t") String token) {
		try {
			Request request = new Request(codR.toString(), admEmail, token);
			// checar token
			if(!JettyLauncher.tokenClass.isAdminTokenValido(request)) {
				return Response.ok().entity( 
						new MsgResponse("request", false, "Token inválido!")
						).build();
			}
			residuoDAO = new ResiduoDAO();
			Residuo residue = residuoDAO.pesquisarResiduo(codR);
			if(residue != null) {
				boolean deleted = residuoDAO.setRemovidoResiduo(residue);
				if(deleted){
					return Response
							.ok()
							.entity(new MsgResponse("remover", true, "Resíduo removido!"))
							.build();
				}
			} else {
				return Response
						.ok()
						.entity(new MsgResponse("remover", false, "Resíduo não existe!"))
						.build();
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return Response
					.status(200)
					.entity( new MsgResponse("remover", false, "Erro na base: " + e.getMessage()) )
					.build();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return Response
				.status(500)
				.entity(
						gson.toJson(new MsgResponse("remover", false, "Erro interno do servidor!"))
				).build();
	}
	    
}

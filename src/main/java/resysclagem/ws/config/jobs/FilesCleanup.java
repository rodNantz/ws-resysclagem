package resysclagem.ws.config.jobs;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.IOUtils;

import resysclagem.ws.classes.Categoria;
import resysclagem.ws.classes.FilePath;
import resysclagem.ws.classes.FilePath.AppendEnum;
import resysclagem.ws.classes.FilePath.PathType;
import resysclagem.ws.classes.Residuo;
import resysclagem.ws.dao.CategoriaDAO;
import resysclagem.ws.dao.ConnectionFactory;
import resysclagem.ws.launch.JettyLauncher;

public class FilesCleanup {
	
	private final static ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
	//private static Process runtimeProcess;
	
	private final static String stmtImgsResiduosBD = "select imagem_residuo from residuo";
	
	/**
	 * 
	 * @param startOneNow
	 */
	public static void scheduleCleanup(boolean startOneNow, String dbUrl, String dbUsuario, String dbSenha){
		// initial Delay the time to delay first execution
		int iDelay = startOneNow ? 0 : 8;
		int execInterval = 24;
	    // period the period between successive executions
		scheduler.scheduleAtFixedRate(
				new Runnable() {
					public void run() {
						try {
							Thread.sleep(10000);
							FilesCleanup.doFileCleanup(
									dbUrl, dbUsuario, dbSenha, false
									);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}, 
				iDelay, execInterval, TimeUnit.HOURS);
	}
	
	public static boolean doFileCleanup(String dbUrl, String dbUser, String dbPass, boolean fromTest) throws Exception {
		
		// Simple things first: checar caminhos d eimagens dos resíduos no banco
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
		Date date = new Date();
		String ts = dateFormat.format(date);
		System.err.println("\nCleanup routine: "+ ts);
		
		Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        List<String> imgsDB = new ArrayList<>();
        try {
            con = ConnectionFactory.getConnection(dbUrl, dbUser, dbPass);
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
            throw ex;
        }
        stmt = con.prepareStatement(stmtImgsResiduosBD);
        rs = stmt.executeQuery();
        
        while(rs.next()){
    		String imgPath = rs.getString("imagem_residuo");
    		imgsDB.add(imgPath);
        }
        //System.err.println(imgsDB);
        stmt.close();
        con.close();

        FilePath fp = new FilePath(PathType.LOCAL, AppendEnum.RESIDUO, fromTest);
        String filesPath = fp.getFullPath();
        System.err.println("Drectory: "+ filesPath);
        
        File folder = new File(filesPath);
        File[] listOfFiles = folder.listFiles();		// all images stored

        for (int i = 0; i < listOfFiles.length; i++) {
        	if (listOfFiles[i].isFile()) {
        		if(!searchForContent(listOfFiles[i].getName(), imgsDB)){
        			System.err.println("DELETE: " + listOfFiles[i].getPath() + " = " + listOfFiles[i].getName());
        			listOfFiles[i].delete();
        		}
        	} else if (listOfFiles[i].isDirectory()) {
        		System.err.println("Directory: " + listOfFiles[i].getName());
        	}
        }
        	
        
        // Now: delete image unused on HTML files?
        // TODO
        
        System.err.println("Finished routine. \n");
		return true;
	}
	
	/*
	 * 
	 */
	public static boolean searchForContent(String fileName, List<String> onList){
		for(String item : onList){
			if(item.contains(fileName)){
				System.err.println("Keep: " + item + " = " +fileName);
				return true;
			}
		}
		return false;
	}

}

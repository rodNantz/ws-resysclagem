package resysclagem.ws.config.jobs;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.attribute.PosixFilePermission;
import java.security.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.EnumSet;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.IOUtils;

import resysclagem.ws.launch.JettyLauncher;

public class DatabaseBackup {
	
	// TODO apply to main and test
	
	private final static ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
	private static Process runtimeProcess;
	
	public static void scheduleBackup(String dbUsuario, String dbSenha, String fullDbUrl){
		// initial Delay the time to delay first execution
		int iDelay = 1;
		int execInterval = 24;
	    // period the period between successive executions
		scheduler.scheduleAtFixedRate(
				new Runnable() {
					public void run() {
						try {
							DatabaseBackup.doBackup(
									dbUsuario, dbSenha, fullDbUrl, "resysclagem"
									);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}, 
				iDelay, execInterval, TimeUnit.HOURS);
	}
	
	public static boolean doBackup(String dbUser, String dbPass, String fullUrl, String dbName) throws Exception {
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
		Date date = new Date();
		String ts = dateFormat.format(date);
		String sqlAddress = getSqlAddress(fullUrl);
		//String fullLocalPath = getFullLocalPath("/dumps/resyclagem-"+ ts +".sql");
		String fullLocalPath = "dumps/resyclagem-"+ ts +".sql";
		
		String[] cmdArr = { 
				"mysqldump",
				"--single-transaction",
				"-h",
				sqlAddress,
				"-u",
                dbUser,
                "-p" + dbPass,
                dbName,
                "-r",
                fullLocalPath
                };
		
		try {
			File sqlFile = new File(fullLocalPath);
			System.out.println(sqlFile.getParent());
			/*Files.setPosixFilePermissions(sqlFile.toPath(), 
					EnumSet.of(PosixFilePermission.OWNER_READ, PosixFilePermission.OWNER_WRITE, PosixFilePermission.OWNER_EXECUTE, PosixFilePermission.GROUP_READ, PosixFilePermission.GROUP_EXECUTE) 
					);*/
					
			sqlFile.mkdirs();
			//sqlFile.createNewFile();
			
			//runtimeProcess = Runtime.getRuntime().exec(new String[] { "cmd.exe", "/c", executeCmd });
			runtimeProcess = new ProcessBuilder(cmdArr)
					.start();
			int processComplete = runtimeProcess.waitFor();
			System.out.println("  Waiting process...");
			if(processComplete == 0){
				System.out.println("* Database Backup taken successfully - " + ts);
				return true;
			} else {
				StringWriter writer = new StringWriter();
				IOUtils.copy(runtimeProcess.getErrorStream(), writer, "UTF-8");
				String errString = writer.toString();
				System.err.println(errString);
				System.err.println("* Could not take mysql backup - " + ts);
			}
		} catch (IOException e) {
			System.err.println("* IOExc - Could not take mysql backup: \n"
					+ ts + "\n"
					+ e.getMessage());
			e.printStackTrace();
		} catch (InterruptedException e) {
			System.err.println("* InterrExc - Could not take mysql backup: \n"
					+ ts + "\n"
					+ e.getMessage());
			e.printStackTrace();
		}
		
		return false;
	}

	
	public static String getFullLocalPath(String path){
		
		//String absolutePath = new File(".").getAbsolutePath();
		//absolutePath = absolutePath.substring(0, absolutePath.length()-2);	//Remove the dot at the end of path
		String absolutePath = System.getProperty("user.home");
		absolutePath += path;
		absolutePath.replace("\\", File.separator);
		absolutePath.replace("/", File.separator);
		return Paths.get(absolutePath).toString();
	}
	
	public static String getSqlAddress(String fullUrl) {
		String strBegin = "mysql://";
		int start = fullUrl.indexOf(strBegin) + strBegin.length();
		int end = fullUrl.indexOf(":3306");
		return fullUrl.substring(start, end);
	}

}

package resysclagem.ws.config.jobs;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import resysclagem.ws.dao.ConnectionFactory;
import resysclagem.ws.dao.ResiduoDAO;

public class ServerURLFilesFixer {
	
	private final static ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
	//private static Process runtimeProcess;
	
	private final static String stmtImgsResiduosBD = "select codr, imagem_residuo from residuo";
	
	/**
	 * 
	 * @param startOneNow
	 */
	public static void scheduleFix(boolean startOneNow, final String dbUrl, final String dbUsuario, final String dbSenha, 
								   String protocol, String currIpAndPort, String fileDir) {
		// initial Delay the time to delay first execution
		int iDelay = startOneNow ? 0 : 2;
		int execInterval = 24;
	    // period the period between successive executions
		scheduler.scheduleAtFixedRate(
				new Runnable() {
					public void run() {
						try {
							Thread.sleep(15 * 1000); 	// 15s
							ServerURLFilesFixer.doFileURLFix(
									dbUrl, dbUsuario, dbSenha, protocol, currIpAndPort, fileDir
									);
						} catch (Exception e) {
							System.err.println("Routine failed: ");
							e.printStackTrace();
						}
					}
				}, 
				iDelay, execInterval, TimeUnit.HOURS);
	}
	
	public static boolean doFileURLFix(String dbUrl, String dbUser, String dbPass, 
									   String protocol, String currIpAndPort, String fileDir) throws Exception {
		// checar caminhos de imagens dos resíduos no banco
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
		Date date = new Date();
		String ts = dateFormat.format(date);
		System.out.println("\nRunning URL fix routine: "+ ts);
		
		Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        Map<Integer, String> imgsDB = new HashMap<>();
        try {
            con = ConnectionFactory.getConnection(dbUrl, dbUser, dbPass);
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
            throw ex;
        }
        stmt = con.prepareStatement(stmtImgsResiduosBD);
        rs = stmt.executeQuery();
        
        while(rs.next()){
        	Integer codR = rs.getInt("codr");
    		String imgUrl = rs.getString("imagem_residuo");
    		imgsDB.put(codR, imgUrl);
        }
        //System.err.println(imgsDB);
        
        stmt.close();
        con.close();
        
        // iterar, separar
        for(int codR : imgsDB.keySet()) {
        	String imgUrl = imgsDB.get(codR);
        	String newUrl = getUpdatedUrl(imgUrl,protocol, currIpAndPort, fileDir);
        	if (newUrl != null &&
        		! imgUrl.equals(newUrl)) 
        	{
        		// UPDATE IT
        		try {
        			alterarImgResiduo(newUrl, codR, dbUrl, dbUser, dbPass);
        			System.out.println(codR +": \n\t from "+ imgUrl +"\n\t to "+ newUrl);
        		} catch (SQLException e) {
        			e.printStackTrace();
        		}
        	}
        }
        
        System.out.println("\nFinished fix routine. \n");
		return true;
	}
	
	
	
	/*
	 * a URL pode ter um IP/domínio e porta desatualizados - esse método retorna a URL com o replace do IP/domínio correto
	 *  
	 */
	private static String getUpdatedUrl(String url, String protocol, String currIpAndPort, String fileDir){
		
		String protUrl = "";
		if(protocol.equalsIgnoreCase("https"))
			protUrl = "https://";
		else
			protUrl = "http://";
		
		try {
			int endingPos = url.indexOf(fileDir);
			String ending = url.substring(endingPos, url.length()); 	// file local name
			
//			String ipAndPort = url.replace(protUrl,  "").replace(ending, "");
//			System.err.println(ipAndPort);
			
			return protUrl + currIpAndPort + ending;
			
		} catch (IndexOutOfBoundsException outEx) {
			System.out.println("\nWarn: String fora dos padrões: "+ url);
		}
		
		return null;
	}
	
	
    private static boolean alterarImgResiduo(String imagemRes, int codR, String dbUrl, String dbUser, String dbPass) 
    		throws SQLException, ClassNotFoundException, Exception 
    {
        Connection con = null;
        PreparedStatement stmt = null;
        try{
            con = ConnectionFactory.getConnection(dbUrl, dbUser, dbPass);
            
            stmt = con.prepareStatement(ResiduoDAO.stmtAlterarImagem, PreparedStatement.RETURN_GENERATED_KEYS);
            stmt.setString(1, imagemRes);
            stmt.setInt(2, codR);
            stmt.executeUpdate();
            
            return true;
        }catch(SQLException ex){
            throw new SQLException("Erro ao alterar o residuo. Origem: " + ex.getMessage());
        }finally{
            try{stmt.close();}catch(Exception ex){
            	System.out.println("Erro ao fechar stmt. Ex = " +ex.getMessage()); throw ex;};
            try{con.close();;}catch(Exception ex){
            	System.out.println("Erro ao fechar conexão. Ex= "+ex.getMessage()); throw ex;};
        }
    }
    

}

package resysclagem.ws.token;

import java.security.MessageDigest;

import com.google.gson.Gson;

import resysclagem.ws.classes.Request;
import resysclagem.ws.classes.Usuario;

public class Resystoken {
	
	private MessageDigest md;
	private String key;
	private boolean tokenSeverity = true;
	
	public Resystoken(String propKey, boolean requireToken) throws Exception {
		try {
			md = MessageDigest.getInstance("SHA-256");
			key = propKey;
			tokenSeverity = requireToken;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	
	/**
	 * Checar se token é válido - aceitando um de usuário ou de admin.
	 * 
	 * @param request
	 * @return
	 */
	public boolean isTokenValido(Request request) {
		if(!tokenSeverity) return true;
		String reqToken = request.getToken(); 
		String sysUToken = getUsuToken(request.getId());
		String sysAToken = getAdminToken(request.getId());
		//debug - Só pra teste!!! comentar
		/*if(!reqToken.equals(sysToken)) {
			System.out.println(reqToken + "\n" 
								+ key + ":" + request.getId() + "\n"
								+ sysToken);
		}*/
		//endebug
		return (reqToken.equals(sysUToken) || reqToken.equals(sysAToken));
	}
	
	public boolean isUsuTokenValido(Request request){
		if(!tokenSeverity) return true;
		String reqToken = request.getToken(); 
		String sysToken = getUsuToken(request.getId());
		//debug - Só pra teste!!! comentar
		/*if(!reqToken.equals(sysToken)) {
			System.out.println(reqToken + "\n" 
								+ key + ":" + request.getId() + "\n"
								+ sysToken);
		}*/
		//endebug
		return reqToken.equals(sysToken);
	}

	public String getUsuToken(String id) {
		MessageDigest userMd = md;
		userMd.update(
				 (key + id)
				 .getBytes());
        return bytesToHex(userMd.digest());
	}
	
	public boolean isAdminTokenValido(Request request){
		if(!tokenSeverity) return true;
		String reqToken = request.getToken(); 
		String sysToken = getAdminToken(request.getId());
		//debug - Só pra teste!!! comentar
		/*if(!reqToken.equals(sysToken)) {
			System.out.println(reqToken + "\n" 
								+ key + ":" + request.getId() + "\n"
								+ sysToken);
		}*/
		//endebug
		return reqToken.equals(sysToken);
	}
	
	public String getAdminToken(String id) {
		MessageDigest userMd = md;
		userMd.update(
				 ("a" + key + id)
				 .getBytes());
        return bytesToHex(userMd.digest());
	}
		
	private String bytesToHex(byte[] bytes) {
	    StringBuffer result = new StringBuffer();
	    for (byte b : bytes) result.append(Integer.toString((b & 0xff) + 0x100, 16).substring(1));
	    return result.toString();
	}
	
}

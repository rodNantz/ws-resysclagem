package resysclagem.ws.utilities;

public class Utility {
	
	/**
	 * Check if is SHA256
	 * @param senha
	 * @return boolean value
	 */
	public static boolean isValidSHA256(String s) {
	    return s.matches("[a-fA-F0-9]{64}");
	}
	
	/**
	 * Check if is md5
	 * @param senha
	 * @return boolean value
	 */
	public static boolean isMd5(String senha)
	{
	    return senha.matches("[a-fA-F0-9]{32}");
	}
	
	public static boolean isNotNull(String txt) {
        return txt != null && txt.trim().length() >= 0 ? true : false;
    }
	
	
}

package resysclagem.ws.utilities;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.io.FileUtils;

import com.google.gson.Gson;

import resysclagem.ws.classes.Dica;
import resysclagem.ws.classes.FilePath;
import resysclagem.ws.classes.FilePath.AppendEnum;
import resysclagem.ws.classes.FilePath.PathType;

public class DicaHtmlBuilder {
	
	private boolean isSucesso = false;
	private String msg = "";
	
	public DicaHtmlBuilder(Dica dica) throws Exception {
		
		if(dica.getAnexos() == null || dica.getAnexos().isEmpty()){
			throw new Exception("Não há anexos a serem adicionados ao HTML!");
		}
		
		FilePath fp = new FilePath(PathType.LOCAL, AppendEnum.DICA);
		String path = fp.getFullPath();
		FilePath fpWS = new FilePath(PathType.SERVICE, AppendEnum.DICA);
		String pathWS = fpWS.getFullPath();
		
		String reutil = "-d";
		if(dica.getIsReutilizavel())
			reutil = "-r";
		
		StringBuilder sb = new StringBuilder();
		sb.append("\uFEFF"							//BOM
				  + "<!DOCTYPE html>"
				  + "<html>"
				  	+ "<head>"
				  		+ "<meta http-equiv=\"Content-Type\" content=\"text/html;charset=ISO-8859-1\">"
				  		+ "<link rel=\"stylesheet\" href=\""+ pathWS +"/style.css\" type=\"text/css\" >"
				  	+ "</head>"
				  	+ "<body>"
				  		+"<div class=\"wrapper\">");
		sb.append("<h3 align=\"center\">"
				   + dica.getNome() 
				   +"</h3>");
		for(String anexo : dica.getAnexos()){
			if(isImage(anexo)){
				sb.append("<div class=\"image\" >" +
							"<img src=\"" + pathWS 										//../webapps/file/dica
							+ "/" + dica.getCodD() 										//	/1 (primary key de dica)
							+ "/" + anexo 												//	/image.png
							+ "\" alt=\"Imagem\" />"		
							+ "</div>");
			} else {
				sb.append("<p class=\"text\"> " + anexo + " </p>");
			}
		}
		sb.append("</div>"
			+ "</body>"
			+ "</html>");
		sb.append("<!-- File created in "	+ Calendar.getInstance().getTime() + " -->");
		
		try {
			
			String completeFileName = path + File.separator + dica.getResiduo().getCodR() + reutil;  //dica/codRediduo-r	
			criarHtml(completeFileName, sb.toString());
			criarUpdateJson(completeFileName, dica.getAnexos());
			setMsg(completeFileName);
			setSucesso(true);
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} 
		
			
	}

	private boolean isImage(String anexo) {
		anexo = anexo.toLowerCase();
		if (   anexo.endsWith(".jpg")
			|| anexo.endsWith(".jpeg")
			|| anexo.endsWith(".png")
			|| anexo.endsWith(".gif")
			|| anexo.endsWith(".bmp")
		){
			return true;
		} else {
			return false;
		}
	}

	public void criarHtml(String fileNome, String html){
		final File f = new File(fileNome + ".html");
        try {
			FileUtils.writeStringToFile(f, html, "UTF-8");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void criarUpdateJson(String fileName, List<String> anexos){
		Gson gson = new Gson();
		String json = gson.toJson(anexos);
		final File f = new File(fileName + ".json");
        try {
			FileUtils.writeStringToFile(f, json, "UTF-8");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public boolean isSucesso() {
		return isSucesso;
	}

	private void setSucesso(boolean isSucesso) {
		this.isSucesso = isSucesso;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}
	
}
